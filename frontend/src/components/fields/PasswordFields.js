import React, {useEffect} from 'react';
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator';

const PasswordFields = ({password, setPassword}) => {

    const [confirmation, setConfirmation] = React.useState("");

    useEffect(() => {
        ValidatorForm.addValidationRule('isPasswordLength', (value) => {
            return value.length >= 8;
        });
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            return value === password;
        });
    });

    const clean = () => {
        setPassword("");
        setConfirmation("");
    };

    return (
        <div>
            <TextValidator
                label="Password"
                name="password"
                type="password"
                onClose={() => clean()}
                onChange={(event) => setPassword(event.target.value)}
                validators={['required', 'matchRegexp:[A-Z]+', 'matchRegexp:[0-9]+', 'isPasswordLength']}
                errorMessages={['this field is required', 'must contains at least one capital', 'must contains at least one digit', 'minimum lenght is 8']}
                value={password}/>
            <TextValidator
                label="Repeat password"
                name="repeatPassword"
                onClose={() => clean()}
                type="password"
                onChange={(event) => setConfirmation(event.target.value)}
                validators={['isPasswordMatch', 'required']}
                errorMessages={['password mismatch', 'this field is required']}
                value={confirmation}/>
        </div>
    )
};

export default PasswordFields;
