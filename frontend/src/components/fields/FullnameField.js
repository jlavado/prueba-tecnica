import React, {useEffect} from 'react';
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator';

const FullnameField = ({fullname, setFullname}) => {

    const clean = () => {
        setFullname("");
    };

    useEffect(() => {
        ValidatorForm.addValidationRule('isFullnameLength', (value) => {
            return value.length <= 200;
        });
    });

    return (
        <div>
            <TextValidator
                label="Fullname"
                name="fullname"
                type="text"
                onClose={clean}
                onChange={(event) => setFullname(event.target.value)}
                validators={['required', 'matchRegexp:^[a-zA-Z\\s]+$', 'isFullnameLength']}
                errorMessages={['this field is required', 'Only letters and spaces', 'Max lenght 200']}
                value={fullname}/>
        </div>
    )
};

export default FullnameField;
