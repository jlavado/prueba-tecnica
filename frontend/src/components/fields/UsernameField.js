import React, {useEffect} from 'react';
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator';

const UsernameField = ({username, setUsername}) => {

    useEffect(() => {
        ValidatorForm.addValidationRule('isUsernameLength', (value) => {
            return value.length <= 20;
        });

        ValidatorForm.addValidationRule('isAvailable', (value) => {
            return value.length !== 0 && fetchUser(value);
        });
    });

    async function fetchUser(username) {
        return fetch('http://localhost:8080/backend/api/users/' + username).then((rs) => rs.status !== 200);
    }

    const clean = () => {
        setUsername("");
    };

    return (
        <div>
            <TextValidator
                label="Username"
                name="username"
                type="text"
                onClose={clean}
                onChange={(event) => setUsername(event.target.value)}
                validators={['required', 'matchRegexp:^[a-zA-Z0-9]+$', 'isUsernameLength', 'isAvailable']}
                errorMessages={['this field is required', 'only alphanumerics', 'Max length 20', 'Username not available']}
                value={username}/>
        </div>
    )
};

export default UsernameField;
