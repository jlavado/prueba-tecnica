import React, {useEffect} from 'react';
import MaterialTable from 'material-table';
import PasswordDialog from "./dialogs/PasswordDialog";
import RegisterUserDialog from "./dialogs/RegisterUserDialog";

const Users = () => {

    const [creation, setCreation] = React.useState(false);
    const [edit, setEdit] = React.useState(false);
    const [user, setUser] = React.useState({});
    const [users, setUsers] = React.useState([]);

    async function fetchUsers() {
        const response = await fetch('http://localhost:8080/backend/api/users');
        const json = await response.json();
        setUsers(json)
    }

    useEffect(() => {
        fetchUsers();
    }, []);

    const closeDialog = (dialog) => () => {
        dialog(false);
        fetchUsers();
    };

    return (
        <div>
            <MaterialTable
                title="User list"
                columns={[
                    {title: 'Name', field: 'fullname'},
                    {title: 'Username', field: 'username'},
                    {title: 'Last Update', field: 'lastUpdate', type: 'datetime'}
                ]}
                data={users}
                actions={[
                    {
                        icon: 'lock',
                        onClick: (event, rowData) => {
                            setUser(rowData);
                            setEdit(true);
                        }
                    },
                    {
                        icon: 'add',
                        isFreeAction: true,
                        onClick: () => {
                            setCreation(true)
                        }
                    }
                ]}/>
            <PasswordDialog user={user} closeDialog={closeDialog(setEdit)} open={edit}/>
            <RegisterUserDialog closeDialog={closeDialog(setCreation)} open={creation}/>
        </div>
    )
};

export default Users;