import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {ValidatorForm} from 'react-material-ui-form-validator';
import PasswordFields from "../fields/PasswordFields";

const PasswordDialog = ({open, closeDialog, user}) => {

    const [password, setPassword] = React.useState("");

    const cleanAndClose = () => {
        setPassword("");
        closeDialog();
    };

    const handleSubmit = () => {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        return fetch('http://localhost:8080/backend/api/users/' + user.username, {
            method: 'put',
            headers: headers,
            body: JSON.stringify({
                username: user.username,
                password: password
            })
        }).then((rs) => {
            if (rs.status === 200) {
                window.confirm("Password updated!")
            }
        }).finally(cleanAndClose);
    };

    return (
        <Dialog open={open} onClose={cleanAndClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Change password for {user.fullname}</DialogTitle>
            <ValidatorForm onSubmit={handleSubmit}>
                <DialogContent>
                    <PasswordFields password={password} setPassword={setPassword}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={cleanAndClose} color="primary">
                        Cancel
                    </Button>
                    <Button color="primary" type="submit">
                        Change password
                    </Button>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
};

export default PasswordDialog
