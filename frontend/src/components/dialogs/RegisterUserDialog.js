import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {ValidatorForm} from 'react-material-ui-form-validator';
import PasswordFields from "../fields/PasswordFields";
import FullnameField from "../fields/FullnameField";
import UsernameField from "../fields/UsernameField";

const RegisterUserDialog = ({open, closeDialog}) => {

    const [username, setUsername] = React.useState("");
    const [fullname, setFullname] = React.useState("");
    const [password, setPassword] = React.useState("");

    const cleanAndClose = () => {
        setFullname("");
        setUsername("");
        setPassword("");
        closeDialog();
    };

    const handleSubmit = () => {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        return fetch('http://localhost:8080/backend/api/users', {
            method: 'post',
            headers: headers,
            body: JSON.stringify({
                username: username,
                fullname: fullname,
                password: password
            })
        }).then((rs) => {
            if (rs.status === 200) {
                window.confirm("User registered!")
            }
        }).finally(cleanAndClose);
    };

    return (
        <Dialog open={open} onClose={cleanAndClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Register new user</DialogTitle>
            <ValidatorForm onSubmit={handleSubmit}>
                <DialogContent>
                    <FullnameField fullname={fullname} setFullname={setFullname}/>
                    <UsernameField username={username} setUsername={setUsername}/>
                    <PasswordFields password={password} setPassword={setPassword}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={cleanAndClose} color="primary">
                        Cancel
                    </Button>
                    <Button color="primary" type="submit">
                        Register User
                    </Button>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
};

export default RegisterUserDialog;
