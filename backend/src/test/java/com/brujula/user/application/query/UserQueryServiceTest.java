package com.brujula.user.application.query;

import com.brujula.user.domain.model.Fullname;
import com.brujula.user.domain.model.Password;
import com.brujula.user.domain.model.User;
import com.brujula.user.domain.model.Username;
import com.brujula.user.domain.service.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class UserQueryServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserQueryService userQueryService;

    @BeforeEach
    public void setup() {
        when(userRepository.findByUsername("chrispeterson")).thenReturn(Optional.of(User.builder()
                .fullname(Fullname.valid("Chris Peterson"))
                .username(Username.valid("chrispeterson"))
                .password(Password.valid("Chr1sPeterson")).build()));
    }

    @Test
    public void isRegistered() {
        assertTrue(userQueryService.isRegisterUser("chrispeterson", "Chr1sPeterson"));
    }

}