package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidPasswordException;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PasswordTest {

    @Test
    void valid() {
        var password = Password.valid("P4ssw0rd");
        assertEquals(password.getValue(), DigestUtils.md5Hex("P4ssw0rd"));
        assertEquals(password, Password.valid("P4ssw0rd"));
    }

    @Test
    void noNumbers() {
        assertThrows(InvalidPasswordException.class, () -> Password.valid("Password"));
    }

    @Test
    void noCapitals() {
        assertThrows(InvalidPasswordException.class, () -> Password.valid("p4ssw0rd"));
    }

    @Test
    void tooShort() {
        assertThrows(InvalidPasswordException.class, () -> Password.valid("assW0rd"));
    }
}