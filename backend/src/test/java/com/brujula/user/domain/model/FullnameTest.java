package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidFullnameException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FullnameTest {

    @Test
    void valid() {
        assertTrue(Fullname.from("José Luis Lavado").isValid());
    }

    @Test
    void invalidChars() {
        assertThrows(InvalidFullnameException.class, () -> Fullname.valid("Urd4ngarín"));
    }

    @Test
    void tooLong() {
        assertThrows(InvalidFullnameException.class, () -> Fullname.valid("Tooooooooooooooooooooooooooooooooooooooo" +
                "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                "oooooooooooooooooo long name"));
    }
}