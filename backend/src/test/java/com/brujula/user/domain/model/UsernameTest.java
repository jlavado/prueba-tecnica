package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidUsernameException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UsernameTest {

    private static final String TOO_LONG = "TooooooooLongUsername";

    @Test
    void valid() {
        assertTrue(Username.from("jll4v4d0").isValid());
    }

    @Test
    void validTrimmed() {
        var username = Username.valid("  jll4v4d0   ");
        assertTrue(username.isValid());
    }

    @Test
    void invalidChars() {
        assertThrows(InvalidUsernameException.class, () -> Username.valid("no valid"));
    }

    @Test
    void almostTooLong() {
        assertTrue(Username.from(TOO_LONG.substring(1)).isValid());
    }

    @Test
    void tooLong() {
        assertThrows(InvalidUsernameException.class, () -> Username.valid(TOO_LONG));
    }
}