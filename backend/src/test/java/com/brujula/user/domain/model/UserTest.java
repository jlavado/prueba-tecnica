package com.brujula.user.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class UserTest {

    @Test
    public void itsMe() {
        var user = User.builder()
                .username(Username.valid("mjackson"))
                .fullname(Fullname.valid("Michael Jackson"))
                .password(Password.valid("Thr1ll3r")).build();

        assertTrue(user.itsMe("mjackson", "Thr1ll3r"));
    }

}