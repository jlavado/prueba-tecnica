package com.brujula.user.infrastructure.persistence;

import com.brujula.user.domain.model.Fullname;
import com.brujula.user.domain.model.Password;
import com.brujula.user.domain.model.User;
import com.brujula.user.domain.model.Username;
import com.brujula.user.domain.service.UserRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Stateless
public class JpaUserRepository implements UserRepository {

    @PersistenceContext(unitName = "backend")
    EntityManager manager;

    @Override
    public void createUser(User user) {
        manager.persist(toEntity(user, null));
    }

    @Override
    public void updateUser(User user) {
        findEntityByUsername(user.getUsername().getValue())
                .map(entity -> toEntity(user, entity.getId()))
                .ifPresent(manager::merge);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return findEntityByUsername(username).map(this::toModel);
    }

    @Override
    public List<User> allUsers() {
        return manager.createNamedQuery("findAll", JpaUserEntity.class).getResultList()
                .stream().map(this::toModel).collect(toList());
    }

    private Optional<JpaUserEntity> findEntityByUsername(String username) {
        try {
            return Optional.of(manager.createNamedQuery("findByUsername", JpaUserEntity.class)
                    .setParameter("username", username).getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    private JpaUserEntity toEntity(User user, Long id) {
        var entity = new JpaUserEntity();
        entity.setId(id);
        entity.setFullname(user.getFullname().getValue());
        entity.setUsername(user.getUsername().getValue());
        entity.setPasswordHash(user.getPassword().getValue());
        return entity;
    }

    private User toModel(JpaUserEntity entity) {
        return User.builder()
                .fullname(Fullname.from(entity.getFullname()))
                .username(Username.from(entity.getUsername()))
                .password(Password.from(entity.getPasswordHash()))
                .lastUpdate(entity.getLastUpdate())
                .build();
    }
}
