package com.brujula.user.infrastructure.rest.config.exception;

import com.brujula.user.domain.exception.DuplicatedUsername;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DuplicatedUserExceptionMapper implements ExceptionMapper<DuplicatedUsername> {
    @Override
    public Response toResponse(DuplicatedUsername duplicatedUsername) {
        return Response.status(Response.Status.CONFLICT).entity(duplicatedUsername.getMessage()).build();
    }
}
