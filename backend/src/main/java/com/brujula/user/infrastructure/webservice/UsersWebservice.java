package com.brujula.user.infrastructure.webservice;

import com.brujula.user.application.query.UserQuery;
import com.brujula.user.application.query.UserQueryService;
import lombok.NoArgsConstructor;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.ForbiddenException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@WebService(serviceName = "UsersWebservice", targetNamespace = "http://xmlns.jllavado.com/services/v1/UsersWebservice")
@NoArgsConstructor
public class UsersWebservice {

    private UserQueryService userQueryService;

    @Resource
    WebServiceContext wsctx;

    @Inject
    public UsersWebservice(UserQueryService userQueryService) {
        this.userQueryService = userQueryService;
    }

    @WebMethod
    @WebResult(name = "users")
    public List<UserQuery> getUserList() {

        MessageContext mctx = wsctx.getMessageContext();
        var headers = (Map<String, List<String>>) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);

        if (isAuthenticated(headers)) {
            return userQueryService.readAllUsers();
        }
        throw new ForbiddenException();
    }

    private boolean isAuthenticated(Map<String, List<String>> headers) {
        var auth = headers.getOrDefault(AUTHORIZATION, emptyList()).stream()
                .filter(it -> it.contains("Basic")).findFirst()
                .map(it -> it.replace("Basic", ""))
                .map(String::trim).map(Base64.getDecoder()::decode).map(Objects::toString)
                .filter(it -> it.contains(":")).orElseThrow(ForbiddenException::new);
        var username = auth.split(":")[0];
        var password = auth.split(":")[0];
        return userQueryService.isRegisterUser(username, password);
    }
}
