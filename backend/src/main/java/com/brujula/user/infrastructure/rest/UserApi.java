package com.brujula.user.infrastructure.rest;

import com.brujula.user.application.command.ChangePasswordCommand;
import com.brujula.user.application.command.RegistrationCommand;
import com.brujula.user.application.command.UserCommandService;
import com.brujula.user.application.query.UserQueryService;
import lombok.NoArgsConstructor;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("users")
@NoArgsConstructor
public class UserApi {

    private UserQueryService userQueryService;

    private UserCommandService userCommandService;

    @Inject
    public UserApi(UserQueryService userQueryService, UserCommandService userCommandService) {
        this.userQueryService = userQueryService;
        this.userCommandService = userCommandService;
    }

    @GET
    @Produces("application/json")
    public Response getAllUsers() {
        return Response.ok(userQueryService.readAllUsers()).build();
    }

    @GET
    @Path("/{username}")
    @Produces("application/json")
    public Response getByUsername(@PathParam("username") String username) {
        return Response.ok(userQueryService.readByUsername(username)).build();
    }

    @PUT
    @Path("/{username}")
    @Consumes("application/json")
    public Response changePassword(ChangePasswordCommand changePasswordCommand) {
        this.userCommandService.changePassword(changePasswordCommand);
        return Response.ok().build();
    }

    @POST
    @Consumes("application/json")
    public Response registerUser(RegistrationCommand registrationCommand) {
        this.userCommandService.registerUser(registrationCommand);
        return Response.ok().build();
    }
}
