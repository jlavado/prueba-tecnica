package com.brujula.user.infrastructure.persistence;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@Data
@Entity
@Table(name = "user", schema = "public")
@NamedQueries({
        @NamedQuery(name = "findAll", query = "select u from JpaUserEntity u"),
        @NamedQuery(name = "findByUsername", query = "select u from JpaUserEntity u where u.username = :username")})
public class JpaUserEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @PreUpdate
    @PrePersist
    public void setLastUpdate() {
        this.lastUpdate = now();
    }


}

