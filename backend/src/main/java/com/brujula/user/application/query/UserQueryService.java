package com.brujula.user.application.query;

import com.brujula.user.domain.model.Password;
import com.brujula.user.domain.model.User;
import com.brujula.user.domain.service.UserRepository;
import com.brujula.user.domain.exception.UserNotFoundException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class UserQueryService {

    private UserRepository userRepository;

    @Inject
    public UserQueryService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserQuery> readAllUsers() {
        return userRepository.allUsers().stream().map(UserQuery::new).collect(toList());
    }

    public UserQuery readByUsername(String username) throws UserNotFoundException {
        return userRepository.findByUsername(username).map(UserQuery::new)
                .orElseThrow(() -> new UserNotFoundException(username));
    }

    public boolean isRegisterUser(String username, String password) {
        var user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        return user.itsMe(username, password);
    }

}
