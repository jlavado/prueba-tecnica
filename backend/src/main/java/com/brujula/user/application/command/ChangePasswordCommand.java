package com.brujula.user.application.command;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChangePasswordCommand {

  private String username;
  private String password;

}
