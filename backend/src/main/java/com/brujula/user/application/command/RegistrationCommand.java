package com.brujula.user.application.command;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegistrationCommand {

  private String username;
  private String fullname;
  private String password;
}
