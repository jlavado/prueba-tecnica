package com.brujula.user.application.query;

import com.brujula.user.domain.model.User;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class UserQuery {
    private final String username;
    private final String fullname;
    private final LocalDateTime lastUpdate;

    public UserQuery(User user) {
        this.username = user.getUsername().getValue();
        this.fullname = user.getFullname().getValue();
        this.lastUpdate = user.getLastUpdate();
    }
}
