package com.brujula.user.application.command;

import com.brujula.user.domain.exception.DuplicatedUsername;
import com.brujula.user.domain.exception.UserNotFoundException;
import com.brujula.user.domain.model.Fullname;
import com.brujula.user.domain.model.Password;
import com.brujula.user.domain.model.User;
import com.brujula.user.domain.model.Username;
import com.brujula.user.domain.service.UserRepository;

import javax.inject.Inject;

public class UserCommandService {

    private final UserRepository userRepository;

    @Inject
    public UserCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void registerUser(RegistrationCommand registrationCommand) {
        if (isUsernameAvailable(registrationCommand.getUsername())) {
            userRepository.createUser(User.builder()
                    .fullname(Fullname.valid(registrationCommand.getFullname()))
                    .username(Username.valid(registrationCommand.getUsername()))
                    .password(Password.valid(registrationCommand.getPassword())).build());
        } else {
            throw new DuplicatedUsername(registrationCommand.getUsername());
        }
    }

    public void changePassword(ChangePasswordCommand changePasswordCommand) {
        var user = userRepository.findByUsername(changePasswordCommand.getUsername())
                .orElseThrow(() -> new UserNotFoundException(changePasswordCommand.getUsername()));
        userRepository.updateUser(user.changePassword(Password.valid(changePasswordCommand.getPassword())));
    }

    private boolean isUsernameAvailable(String username) {
        return userRepository.findByUsername(username).isEmpty();
    }
}
