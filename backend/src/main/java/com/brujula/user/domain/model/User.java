package com.brujula.user.domain.model;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@Value
@Builder
public class User {

    Fullname fullname;
    Username username;
    Password password;
    LocalDateTime lastUpdate;

    public User changePassword(Password password) {
        return builder().fullname(fullname).username(username).password(password).lastUpdate(now()).build();
    }

    public boolean itsMe(String username, String password) {
        return Username.from(username).equals(this.username) && Password.ingest(password).equals(this.password);
    }
}

