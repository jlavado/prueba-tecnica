package com.brujula.user.domain.service;

import com.brujula.user.domain.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    void createUser(User user);

    void updateUser(User user);

    Optional<User> findByUsername(String username);

    List<User> allUsers();
}
