package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidUsernameException;
import lombok.Value;

@Value(staticConstructor = "from")
public class Username {

    private static final int MAX_USERNAME_LENGTH = 20;

    String value;

    public static Username valid(String value) {
        var username = Username.from(value.trim());
        if (username.isValid()) return username;
        else throw new InvalidUsernameException(value);
    }

    public boolean isValid() {
        return !value.isBlank() && value.length() <= MAX_USERNAME_LENGTH &&
                value.chars().allMatch(i -> Character.isDigit(i) || Character.isAlphabetic(i));
    }
}
