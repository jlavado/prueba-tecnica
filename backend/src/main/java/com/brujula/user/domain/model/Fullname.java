package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidFullnameException;
import lombok.Value;

@Value(staticConstructor = "from")
public class Fullname {

    private static final int MAX_FULLNAME_LENGTH = 200;

    String value;

    public static Fullname valid(String value) {
        var fullname = from(value.trim());
        if (fullname.isValid()) return fullname;
        else throw new InvalidFullnameException(value);
    }

    public boolean isValid() {
        return !value.isBlank() && value.length() <= MAX_FULLNAME_LENGTH &&
                value.chars().allMatch(i -> Character.isSpaceChar(i) || Character.isAlphabetic(i));
    }
}
