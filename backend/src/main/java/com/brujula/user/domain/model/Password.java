package com.brujula.user.domain.model;

import com.brujula.user.domain.exception.InvalidPasswordException;
import lombok.Value;
import org.apache.commons.codec.digest.DigestUtils;

@Value(staticConstructor = "from")
public class Password {

    private static final int MINIMUM_PASSWORD_LENGTH = 8;

    String value;

    public static Password valid(String value) {
        var password = from(value.trim());
        if (password.isValid()) return Password.ingest(value);
        else throw new InvalidPasswordException();
    }

    public static Password ingest(String value) {
        return Password.from(DigestUtils.md5Hex(value));
    }

    private boolean isValid() {
        return value.length() >= MINIMUM_PASSWORD_LENGTH &&
                value.chars().anyMatch(Character::isDigit) &&
                value.chars().anyMatch(Character::isUpperCase);
    }
}
