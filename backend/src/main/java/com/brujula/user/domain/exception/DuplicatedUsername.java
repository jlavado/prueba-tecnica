package com.brujula.user.domain.exception;

public class DuplicatedUsername extends RuntimeException {
    public DuplicatedUsername(String username) {
        super("The username '" + username + "' is not available");
    }
}
