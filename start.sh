#!/bin/sh
docker run -it -v $(pwd)/frontend:/app -w /app node npm install
docker run -it -v $(pwd)/backend:/app -w /app maven:3.6.3-jdk-11 mvn clean install
docker-compose up --build
